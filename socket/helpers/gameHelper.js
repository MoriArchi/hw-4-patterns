import {
  roomExists,
  getRoomDetails,
  sendRoomsList,
  clearRoom,
  getRoomEntity,
  toggleRoomAvailabilityIfNeeds
} from './roomHelper';
import * as config from '../config';
import { availableRooms } from '..';


export const generateWinnersList = (io, roomName, startedAt) => {
  if (!roomExists(io, roomName)) {
    return;
  }

  const players = getRoomDetails(io, roomName).users;
  return players.sort((player1, player2) => {
    if (player1.progress === player2.progress) {
      if (player1.lastPress && player2.lastPress) {
        return player1.lastPress - player2.lastPress;
      }
    }
    return player2.progress - player1.progress;
  })
    .map(({ name, lastPress, progress }) => {
      const isTextCompleted = progress === 1;

      let timeSpent = isTextCompleted ? lastPress - startedAt : startedAt + config.SECONDS_FOR_GAME;

      timeSpent = (timeSpent / 1000).toFixed(4); 
      const percentageProgress = (progress * 100).toFixed(2);
      
      return {
        name,
        timeSpent,
        isTextCompleted,
        percentageProgress
      }
    });
};

export const scheduleGameIfNeeds = (io, roomName) => {
  if (!roomExists(io, roomName)) {
    return;
  }

  const users = getRoomDetails(io, roomName).users;
  if (users.some(user => !user.ready)) {
    return;
  };

  const roomEntity = getRoomEntity(io, roomName);

  if(roomEntity.game.isNotScheduled()) {
    scheduleGame(io, roomEntity);

    const { countdown, secsToGameEnd  } = getRoomEntity(io, roomName).game

    io.in(roomName).emit('GAME_SCHEDULED', { countdown, secsToGameEnd });
  }
};

export function scheduleGame(io, room) {
  delete availableRooms[room.name];
  sendRoomsList(io, io);

  room.game.scheduleGame();
}

export const checkRoomForEnding = (io, roomName, socket) => {
  if (!roomExists(io, roomName)) return;

  const users = getRoomDetails(io, roomName).users;
  if (users.every(user => user.progress === 1)) {
    const room = getRoomEntity(io, roomName);
    const gameStartedAt = room.game.gameStartedAt;
    room.commentator.produceMessage('raceEnd', { participants: users, gameStartedAt: gameStartedAt});
    room.game.resetGame();
    room.game.clearIntervals();

    io.in(roomName).emit('END_GAME');
    clearRoom(io, roomName);
    toggleRoomAvailabilityIfNeeds(io, roomName);
    sendRoomsList(io, socket.broadcast);
  }
}

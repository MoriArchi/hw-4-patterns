import * as config from './config';
import useMiddleware from './middleware';

import {
  getRoomEntity,
  getRoomDetails,
  roomExists,
  sendRoomsList,
  updateRoom,
  joinSocketToRoom,
  clearRoom,
  toggleRoomAvailabilityIfNeeds
} from './helpers/roomHelper';

import {
  generateWinnersList,
  scheduleGameIfNeeds,
  checkRoomForEnding
} from './helpers/gameHelper';

import { Game } from '../models/game';
import { Commentator } from '../models/commentator';

export const availableRooms = {}

export default io => {
  useMiddleware(io);

  const updateRoomIfExists = (io, roomName) => {
    if (roomExists(io, roomName)) {
      updateRoom(io, roomName);
    }
  };

  io.on('connection', socket => {
    const username = socket.handshake.query.username;

    socket.userData = {
      name: username,
      ready: false,
      progress: 0,
      lastPress: null,
      typed: 0,
      car: undefined
    };

    sendRoomsList(io, socket);

    socket.on('disconnect', () => {
      const { roomName } = socket;

      updateRoomIfExists(io, roomName);

      toggleRoomAvailabilityIfNeeds(io, roomName);

      sendRoomsList(io, socket.broadcast);
      scheduleGameIfNeeds(io, roomName);
      checkRoomForEnding(io, roomName, socket);
    });

    socket.on('NEW_ROOM', (roomName, callback) => {
      if (roomExists(io, roomName)) {
        callback(`Room with name ${roomName} already exists`);
        return;
      }

      joinSocketToRoom(io, socket, roomName, callback);
      availableRooms[roomName] = true;

      const room = getRoomEntity(io, roomName);

      room.name = roomName;
      room.commentator = new Commentator({ io, roomName });

      room.game = new Game({
        onGameEnd: (gameStartedAt) => {
          getRoomEntity(io, roomName).commentator.produceMessage('raceEnd', { participants: getRoomDetails(io, roomName).users, gameStartedAt: gameStartedAt });

          io.in(roomName).emit('END_GAME');
          clearRoom(io, roomName);
          toggleRoomAvailabilityIfNeeds(io, roomName);
          sendRoomsList(io, socket.broadcast);
        },
        onUpdateCountdown: (countdown) => {
          io.in(roomName).emit('UPDATE_COUNTDOWN', countdown);
        },
        onStartGame: ({ text, gameStartedAt }) => {
          getRoomEntity(io, roomName).commentator.produceMessage('raceStart', { participants: getRoomDetails(io, roomName).users, gameStartedAt: gameStartedAt });

          io.in(roomName).emit('START_GAME', text);
        },
        onUpdateGameTimer: ({ currentSecsToGameEnd, defaultSecsToGameEnd }) => {
          if (roomExists(io, roomName)) {
            if ((defaultSecsToGameEnd - currentSecsToGameEnd) % 30 === 0) {
              getRoomEntity(io, roomName).commentator.produceMessage('raceState', { participants: getRoomDetails(io, roomName).users });
            }

            if (Math.random() < 0.35) {
              getRoomEntity(io, roomName).commentator.produceMessage('randomInfo');
            }
            io.in(roomName).emit('UPDATE_GAME_TIMER', currentSecsToGameEnd);
          }
        },
      });

    });

    socket.on('JOIN_ROOM', ({ roomName }, callback) => {
      const roomLength = getRoomEntity(io, roomName).length;

      toggleRoomAvailabilityIfNeeds(io, roomName);

      if (roomLength === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        callback('Maximum number of players in the room is reached');
        return;
      }

      joinSocketToRoom(io, socket, roomName, callback);
    });

    socket.on('LEAVE_ROOM', () => {
      const roomName = socket.roomName;

      socket.leave(roomName, () => {
        socket.roomName = null;

        if (roomExists(io, roomName)) {
          updateRoom(io, roomName);
          scheduleGameIfNeeds(io, roomName);
        }

        toggleRoomAvailabilityIfNeeds(io, roomName);

        sendRoomsList(io, io);
      });
    });

    socket.on('TOGGLE_READY', () => {
      socket.userData.ready = !socket.userData.ready;

      updateRoomIfExists(io, socket.roomName);
      scheduleGameIfNeeds(io, socket.roomName);
    });

    socket.on('USER_TYPING', (key) => {
      const { roomName, userData } = socket;

      const { text } = getRoomEntity(io, roomName).game;

      const typedLettersAmount = userData.typed;

      if (text.length - typedLettersAmount === 30) {
        getRoomEntity(io, roomName).commentator.produceMessage('preFinish', { finishingParticipantName: userData.name });
      }

      if (text.length === typedLettersAmount) {
        getRoomEntity(io, roomName).commentator.produceMessage('finish', { finishedParticipantName: userData.name });
        return;
      }

      if (text[typedLettersAmount] === key) {
        socket.userData.lastPress = Date.now();
        socket.userData.typed++;
        socket.userData.progress = socket.userData.typed / text.length;

        io.in(roomName).emit('UPDATE_PROGRESS', socket.userData.name, socket.userData.progress);

        const typedText = text.substring(0, socket.userData.typed);

        const nextLetter = text.length === socket.userData.typed
          ? ''
          : text[socket.userData.typed];

        const textLeft = text.length > socket.userData.typed + 1
          ? text.substring(socket.userData.typed + 1)
          : '';

        socket.emit('UPDATE_USER_TYPED_TEXT', { typedText, nextLetter, textLeft });

        checkRoomForEnding(io, roomName, socket)
      }
    })

  });
};
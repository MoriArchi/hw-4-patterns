'use script';

import { InformationalMessage } from './informationalMessage';

const CAR_ADJECTIVES = ['shiny', 'beautiful', 'legendary', 'ultimate', 'fast', 'rusty', 'dusty'];

export class RaceStartMessage extends InformationalMessage {
    constructor(participants, gameStartedAt) {
        super(participants, gameStartedAt);
    }

    getMessageText() {
        return this.participants.reduce((messageString, participant, index) => { // Built-in higher-order function "reduce"
            return `${messageString}On the ${index + 1}${this.getOrdinalLetter(index + 1)} place - ${participant.name} in his/her ${this.getRandomCarAdjective()} ${participant.car}. `;
        }, 'Hi everyone, we are Jerry and Michael, hosts of today\'s race! And here\'s the starting grid. ');
    }

    getRandomCarAdjective() {
        return CAR_ADJECTIVES[Math.floor(Math.random() * CAR_ADJECTIVES.length)];
    }
}
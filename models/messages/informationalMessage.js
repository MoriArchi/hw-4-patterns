'use script';

import { CommentatorMessage } from './сommentatorMessage';

export class InformationalMessage extends CommentatorMessage {
    constructor(participants, gameStartedAt) {
        super();
        this.participants = participants;
        this.gameStartedAt = gameStartedAt;

        this.sortParticipants();
    }

    getOrdinalLetter(number) {
        if (number < 1) {
            return;
        } else if (number % 10 === 1) {
            return 'st';
        } else if (number % 10 === 2) {
            return 'nd';
        } else if (number % 10 === 3) {
            return 'rd';
        } else {
            return 'th';
        }
    }

    sortParticipants() {
        this.participants.sort((participantA, participantB) => { // Built-in higher-order function "sort"
            if (!participantA.lastPress) return 1;
            if (!participantB.lastPress) return -1;

            const symbolsDiff = participantB.typed - participantA.typed;
            if (!symbolsDiff) {
                if (participantA.lastPress && participantB.lastPress) {
                    return participantA.lastPress - participantB.lastPress;
                }
            }
            return symbolsDiff;
        });
    }
}

export const generateWinnersList = (io, roomName, startedAt) => {
    if (!roomExists(io, roomName)) {
      return;
    }
  
    const players = getRoomDetails(io, roomName).users;
    return players.sort((player1, player2) => {
      if (player1.progress === player2.progress) {
        if (player1.lastPress && player2.lastPress) {
          return player1.lastPress - player2.lastPress;
        }
      }
      return player2.progress - player1.progress;
    })
      .map(({ name, lastPress, progress }) => {
        const isTextCompleted = progress === 1;
  
        let timeSpent = isTextCompleted ? lastPress - startedAt : startedAt + config.SECONDS_FOR_GAME;
  
        timeSpent = (timeSpent / 1000).toFixed(4); 
        const percentageProgress = (progress * 100).toFixed(2);
        
        return {
          name,
          timeSpent,
          isTextCompleted,
          percentageProgress
        }
      });
  };
'use script';

import { CommentatorMessage } from './сommentatorMessage';

export class FinishMessage extends CommentatorMessage {
    constructor(finishedParticipantName) {
        super();
        this.finishedParticipantName = finishedParticipantName;
    }

    getMessageText() {
        return `${this.finishedParticipantName} finished the race!`;
    }
}
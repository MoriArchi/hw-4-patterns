'use script';

import { InformationalMessage } from './informationalMessage';

export class RaceStateMessage extends InformationalMessage {
    constructor(participants) {
        super(participants);
    }

    getMessageText() {
        return this.participants.reduce((messageString, { name, typed }, index) => { // Built-in higher-order function "reduce"
            return `${messageString}${name} is on the ${index + 1}${this.getOrdinalLetter(index + 1)} place with the ${typed} entered symbols. `;
        }, 'Okay folks, what do we have here... ');
    }
}
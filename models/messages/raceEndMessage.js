'use script';

import prettyMs from 'pretty-ms';
import { InformationalMessage } from './informationalMessage';

export class RaceEndMessage extends InformationalMessage {
    constructor(participants, gameStartedAt) {
        super(participants, gameStartedAt);
    }

    getMessageText() {
        const prizePlaces =  this.participants.slice().reduce((messageString, { name, lastPress }, index) => { // Built-in higher-order function "reduce"
            let participantResult;
            if (!lastPress) {
                participantResult = `${name} didn't make it to the end. `
            } else {
                participantResult = `${name} ended on the ${index + 1}${this.getOrdinalLetter(index + 1)} place with the time of ${((lastPress - this.gameStartedAt ) / 1000).toFixed(2)} seconds. `;
            }
            return `${messageString}${participantResult}`;
        }, 'Okay that was an intensive race! What we got in result... ');

        return `${prizePlaces} It was pleasure for us to host this race. See y'all later!`
    }
}
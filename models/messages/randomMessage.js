'use script';

import { CommentatorMessage } from './сommentatorMessage';

export class RandomMessage extends CommentatorMessage {
    constructor(messageList) {
        super();
        this.messageList = messageList;
    }

    getMessageText() {
        return this.messageList[Math.floor(Math.random() * this.messageList.length)];
    }
}
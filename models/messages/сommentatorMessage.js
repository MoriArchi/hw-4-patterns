'use script';

export class CommentatorMessage { // Base interfase
  constructor() {
  }

  getMessageText() { // Virtual method
    return 'Default message';
  }
}
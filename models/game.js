import data from '../data';
import { getRandomElFromArray } from '../socket/helpers/common';
import * as config from '../socket/config';

export class Game {
  onStartGame;
  onUpdateCountdown;
  onUpdateGameTimer;
  onGameEnd;
  gameStartedAt = null;
  countdown = config.SECONDS_TIMER_BEFORE_START_GAME;
  secsToGameEnd = config.SECONDS_FOR_GAME;
  countdownIntervalId = null;
  gameEndIntervalId = null;
  gameStartedAt = null;
  text = getRandomElFromArray(data);

  constructor({ onStartGame, onUpdateCountdown, onUpdateGameTimer, onGameEnd }) {
    Object.assign(this, { onStartGame, onUpdateCountdown, onUpdateGameTimer, onGameEnd });
  }

  scheduleGame() {
    this.countdownIntervalId = setInterval(() => {
      this.countdown--;
      this.onUpdateCountdown(this.countdown);

      if (this.countdown <= 0) {
        this.countdown = null;
        clearInterval(this.countdownIntervalId);

        this.startGame(this.text);
      }
    }, 1000)

  }

  isNotScheduled() {
    return !this.isPreparingForGame() && !this.isGameInProcess();
  }

  isPreparingForGame() {
    return this.countdownIntervalId !== null;
  }

  isGameInProcess() {
    return this.gameEndIntervalId !== null;
  }

  startGame() {
    this.gameStartedAt = Date.now();
    this.text = getRandomElFromArray(data);

    this.onStartGame({text: this.text, gameStartedAt: this.gameStartedAt });

    this.gameEndIntervalId = setInterval(() => {
      this.secsToGameEnd -= 1;
      this.onUpdateGameTimer({ currentSecsToGameEnd: this.secsToGameEnd, defaultSecsToGameEnd: config.SECONDS_FOR_GAME });

      if (this.secsToGameEnd <= 0) {
        clearInterval(this.gameEndIntervalId);
        this.onGameEnd(this.gameStartedAt);
        this.resetGame();
      }

    }, 1000);
  }

  resetGame() {
    this.countdown = config.SECONDS_TIMER_BEFORE_START_GAME;
    this.secsToGameEnd = config.SECONDS_FOR_GAME;

    this.clearIntervals();

    this.countdownIntervalId = null;
    this.gameEndIntervalId = null;
    this.gameStartedAt = null;
  }

  clearIntervals() {
    clearInterval(this.countdownIntervalId);
    clearInterval(this.gameEndIntervalId);
  }
}